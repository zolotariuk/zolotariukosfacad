$(function() {

//Start Of Manipulation With From
	var $loginButton = $('.main-header__login').find('a');
	var $loginForm = $('#login-form');
	var $registerForm = $('#register-form');

//Open Login And Register Form
	$loginButton.first().magnificPopup();
	$loginButton.last().magnificPopup();

//Change Forms Inside Open Popup
	$('.form-controlers__login').find('a').click(function(){$loginForm.show()}).magnificPopup();
	$('.form-controlers__register').find('a').click(function(){$registerForm.show()}).magnificPopup();

//Show Form On Click
	$loginButton.first().click(function(){
		$loginForm.show();
	});
	$loginButton.last().click(function(){
		$registerForm.show();
	});


// Slider Options
	$('.owl-carousel').owlCarousel({
		items:1,
		loop:true,  
		dots:true,
		autoplay:true,
		autoplayHoverPause: true,
		autoplayTimeout: 3000, 
		smartSpeed:1000, 
		touchDrag:false,
		mouseDrag:false,
		pullDrag:false,
		freeDrag:false,
		dotsContainer: '.carousel-navigation',
	});

// Change Active Status In Main Menu
	var $mainNavItem = $('.main-nav-links__item');
	var $MainNavActive = "main-nav--active";
	$mainNavItem.click(function(){
    $mainNavItem.removeClass($MainNavActive);
    $(this).addClass($MainNavActive);
	});

// Count And Display of Cart and Star Click
	var $totalCart = 0;
	var $totalStar = 0;
	var $productIconActive = 'product-icons__active';
	var $mainNavCartLi = $('.main-nav-cart li');
	var $productsIcons = $('.product-icons');

// Count Stars	
	$productsIcons.find('.fa-star').click(function(){
		event.preventDefault();
		if ($(this).hasClass($productIconActive)){
			$totalStar-=1
			$(this).removeClass($productIconActive);
			$mainNavCartLi.first().find('a').text($totalStar);
		}else{
			$totalStar+=1
			$(this).addClass($productIconActive);
			$mainNavCartLi.first().find('a').text($totalStar);
		}
	});

//Count Carts
	$productsIcons.find('.fa-shopping-cart').click(function(){
		event.preventDefault();
		if ($(this).hasClass($productIconActive)){
			$totalCart-=1
			$(this).removeClass($productIconActive);
			$mainNavCartLi.last().find('a').text($totalCart);
		}else{
			$totalCart+=1
			$(this).addClass($productIconActive);
			$mainNavCartLi.last().find('a').text($totalCart);
		}
	});

//Start of Manipulation With Serch Fild
	var $searchField = $('#searchfield');
	var $dropDownSearch = $('.search-dropdown');
	var $mainHeaderSearch =$('.main-header__search').find('span');

//After FocusOut Show Icon and Placeholder
	$searchField.on( "focusout", function() {
  	$mainHeaderSearch.show();
  	$searchField.val('');
	});

//Hide Icon
	$searchField.on( "keydown", function() {
  	$mainHeaderSearch.hide();
	});

//Show and Hide icons on Products View
	var $productPrevieTitle = $('.product-preview-imgs__title');

	$productPrevieTitle.hover(function() {
  	$(this).find('p').hide();
  	$(this).find($productsIcons).show();
	});
	$productPrevieTitle.mouseleave(function() {
		$(this).find($productsIcons).hide();
  	$(this).find('p').show();	
	});

//Change news every 5 searchfield
	var $blogNewsItem = $('.blog-news-wrapper');

	setInterval(function(){
		$blogNewsItem.first().is(":visible") ? $blogNewsItem.first().fadeOut() : $blogNewsItem.first().delay( 450 ).fadeIn();
		$blogNewsItem.last().is(":visible") ? $blogNewsItem.last().fadeOut() : $blogNewsItem.last().delay( 450 ).fadeIn();
	},5000);

// Show Img From Image Widget in Full Size
	$('.img-widget__item').find('a').magnificPopup({type:'image'});


// Store Emails in Json (Consol Log)
	$('#email-news-form').on('submit', function(e){
		var jsonData = {};
		var formData = $("#email-news-form").serializeArray();
	  // console.log(formData);
	  this.reset();
	  alert("Thank you for provide your Email");
	  $.each(formData, function() {
	  	if (jsonData[this.name]) {
	  		if (!jsonData[this.name].push) {
	  			jsonData[this.name] = [jsonData[this.name]];
	  		}
	  		jsonData[this.name].push(this.value || '');
	  	} else {
	  		jsonData[this.name] = this.value || '';
	  	}
	  });
	  console.log(jsonData);
	  e.preventDefault(); 
  });



// Selectize for Sort By section
  $('select').selectize();

//Filters for Products Page
	var $filterButton = $(".product-preview-header--products-page__filters--buttons").find('li');
	var $productPrewievContent = $(".product-preview-imgs__item--content");
	var $activeProductPrewiev = "active--product-prewiev";

  $filterButton.last().click(function(){
  	$productPrewievContent.addClass("d-flex");
  });

  $filterButton.first().click(function(){
  	$productPrewievContent.removeClass("d-flex");
  });

//Change active class on filter button
  $filterButton.click(function(){
  	$filterButton.removeClass($activeProductPrewiev);
  	$(this).addClass($activeProductPrewiev);
  });

  var $hamburger = $(".hamburger");
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
    $('.main-nav-links').toggle();
  });


  var options = {
  	url: "/autocomplite.json",

  	getValue: "name",

  	list: {
  		maxNumberOfElements: 2,
  		match: {
  			enabled: true
  		}
  	}
  };

// Sarch for small device
	
	var $searchfieldForSmallDevice = $("#searchfield_for-small-device");
	var $mainHeaderSearch = $('.main-header__search');

  $("#searchfield").easyAutocomplete(options);
  $searchfieldForSmallDevice.easyAutocomplete(options);
  

  $('.search-on-small-screen').children('span').on("click", function(e) {
  	$searchfieldForSmallDevice.val('');
    $mainHeaderSearch.toggleClass('hidden');
    $mainHeaderSearch.submit(function() {
    	$mainHeaderSearch.addClass('hidden');
    	$searchfieldForSmallDevice.val('');
    });
  });

  $('.selectize-input > input').prop('disabled', 'disabled');

});


